// Esfera.cpp: implementation of the Esfera class.
//Prueba escritura en cabecera 
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=1.0f;
	velocidad.x=3;
	velocidad.y=3;
	ft=1/35.0f;//cte de tiempo=1/ft => cte tiempo=35.0;  si queremos anular el efecto => ft=0;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+velocidad*t;
	if(radio>0.1){
		radio=radio-(t*ft);
	}
	else{radio=0.1;}
}

