# Changelog
Todos los cambios de este proyecto serán documentados en este fichero.

##[Unreleased]

## [v1.2] - 2020-10-22
### Added
-Límite de tamaño mínimo para la pelota

## [v1.2] - 2020-10-14
### Changed
-Mejora reducción pelota

## [v1.2] - 2020-10-14
### Added
-El tamaño de la pelota disminuye con el tiempo

## [v1.2] - 2020-10-14
### Added
-Movimiento de la raqueta mediante el teclado y de la pelota

## [v1.2]  - 2020-10-8
### Changed
-Segunda modificación de cabeceras

## [v1.1] - 2020-9-23
### Changed
- Primera modificación de las cabeceras de algunos ficheros

##[v1.0] 2020-10-7
### Added
-Clonación del codigo inicial

[Unreleased]:https://bitbucket.org/angel_mmolina/sii-54100
